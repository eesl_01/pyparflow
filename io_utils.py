"""
Tecplot IO functions. Useful for plotting PARFLOW-CLM, TPROGS & HYVR.
Created on Tuesday Feb 02 2018
@author: Emilio Sanchez-Leon, Uni Tubingen
@comments: Check to do list!
"""
import numpy as np
import os, re


# todo: integrate routine to convert ascii to binary tecplot file for computing concerns:
def find_exact_shape(node_number, el_number, el_lines=100, node_lines=8800, max_iters=1000):
    """ Find divisor without remainder to reshape data for Tecplot output file.
    :param node_number: int, total number of nodes.
    :param el_number:   int, total number of elements.
    :param el_lines:    int, approx. no of lines for elemental data in output file. Default=100
    :param node_lines:  int, approx. no of lines for nodal data in output file. Default=100
    :param max_iters:   int, number of iteration for searching for proper divisor.
    :return: adequate node_lines, el_lines closest to the input values, for a proper reshape.
    """
    iters = 1  # initialize counter
    Deni = Dend = el_lines  # vars for searching increasing and decreasing denominators
    iters_node = 1
    nodeDeni = nodeDend = node_lines

    while el_number % el_lines != 0:
        Deni += 1  # searching increased vals
        if Dend > 0: Dend -= 1  # searching decreased vals, but check 0 condition
        if el_number % Dend == 0:  # found a 0 remainder denominator
            el_lines = Dend  # assign found denominator
            break
        elif el_number % Deni == 0:  # found a 0 remainder denominator
            el_lines = Deni  # assign found denominator
            break
        elif iters >= max_iters:  # check loop count
            break
        iters += 1

    while node_number % node_lines != 0:
        nodeDeni += 1  # searching increased vals
        if nodeDend > 0: nodeDend -= 1  # searching decreased vals, but check 0 condition
        if node_number % nodeDend == 0:  # found a 0 remainder denominator
            node_lines = nodeDend  # assign found denominator
            break
        elif node_number % nodeDeni == 0:  # found a 0 remainder denominator
            node_lines = nodeDeni  # assign found denominator
            break
        elif iters_node >= max_iters:  # check loop count
            break
            iters_node += 1

    return node_lines, el_lines


def str_infile(myfile, mystr, index=False):
    """ Find the defined string plus one. The string of interest should
    be able to be converted to "FLOAT" number(s). File format is any.
    :param  myfile: str, full path of the file of interest
    :param  mystr: str, string BEFORE the one of interest
    :param  index: bool, whether to return the line index where string was found
    :return:    string located at the next position than mystr. If index is True returns also
    the line indices where there was a match.
    """
    next_str = []
    ids = []
    with open(myfile, 'r') as fp:
        # counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr + '([^,]+)', line)
            if match:
                ids.append(idx)
                next_str.append(match.group(1))
    if len(next_str) == 1:
        next_str = next_str[0]
        ids = ids[0]
    else:
        try:
            next_str = np.asarray(next_str, dtype='float')
            ids = np.asarray(ids, dtype='int')
        except:
            next_str = np.asarray(next_str, dtype='str')
            ids = np.asarray(ids, dtype='int')

    if len(next_str) > 0:
        if index is False:
            return next_str
        elif index is True:
            return ids, next_str
    else:
        print('Given string not found in file')


def rd_tcplotmesh(my_meshfile, fulloutput=False, gridformat='tp2014'):
    """ Read relevant information from model grid. Supports files with Tecplot and gms formats.
    :param my_meshfile:    str, full directory of the grid source file
    :param fulloutput:     bool, opt. If True, returns also the actual nodes coordinates and nodes number for each element
    :param gridformat:     str, format of the grid source file. ['tp2014','tp2018', 'gms'], Default is 'tp2014'
    :return:    nnodes, nelem. If fulloutput is True also nodes, elements
    """
    assert os.path.exists(my_meshfile), 'Mesh file does not exists'
    if 'tp' in gridformat:
        nnodes = int(str_infile(my_meshfile, 'N='))
        nelem = int(str_infile(my_meshfile, 'E='))

    elif gridformat == 'gms':
        temp1 = np.genfromtxt(my_meshfile, skip_header=4, dtype=None, usecols=(0,))
        nnodes = len(np.where(temp1 == b'ND')[0])
        nelem = len(np.where(temp1 == b'E8H')[0])

    if fulloutput is True:
        if gridformat == 'tp2014':
            nodes_coord = np.genfromtxt(my_meshfile, skip_header=3, skip_footer=nelem, usecols=(0, 1, 2))
            element_nodes = np.loadtxt(my_meshfile, skiprows=nnodes + 3)
        elif gridformat == 'tp2018':
            print('under construction') # todo: finish this function
        elif gridformat == 'gms':
            nodes_coord = np.loadtxt(my_meshfile, skiprows=4+nelem, usecols=(2, 3, 4))
            element_nodes = np.genfromtxt(my_meshfile, skip_header=4, skip_footer=nnodes, usecols=(2, 3, 4, 5, 6, 7, 8, 9))
        assert element_nodes.shape[0] == nelem, 'Mismatch with element number'
        assert nodes_coord.shape[0] == nnodes, 'Mismatch with nodes number'

    if fulloutput is True:
        return nnodes, nelem, nodes_coord, element_nodes
    elif fulloutput is False:
        return nnodes, nelem


def rd_bgr_tprogs(myfile, nx_el, ny_el, nz_el, dx=1, dy=1, dz=1, mark_data=False, reshape=False, tp_arrays=True):
    """ Read simulation binary file(*.bgr) generated by TPROGS.
    Note: NCOL Number of X el, NROW Number of Y el, NLAYNumber of Z el.
    :param myfile:  str, file path\name of interest
    :param nx_el:   int, number of elements in X direction
    :param ny_el:   int, number of elements in Y direction
    :param nz_el:   int, number of elements in Z direction
    :param dx:      int, element size in X dimension
    :param dy:      int, element size in Y dimension
    :param dz:      int, element size in Z dimension
    :param mark_data:   bool, if False use np.abs() to remove negative values for cells with conditioned data
    :param reshape:     bool, reshape the array into NX x NY x NZ if True
    :param tp_arrays:   bool, generate grid arrays in Tecplot order for tecplot-ASCII file
    :return: alldata, numpy array with the categorical data generated by TPROGS. If tp_arrays, returns also grid array
    :example:
        >>> alldata, nodal_data = rd_bgr_tprogs(os.path.join('_data', 'reactive_sim3d.bgr'), 100, 50, 3, dx=1, dy=1, dz=1, mark_data=False, reshape=False, tp_arrays=True)
        >>> wr_totecplot(nodal_data, alldata, nodes_in_x=101,nodes_in_y=51,nodes_in_z=4,var_names=['X','Y','Z','IDX'], outputfile=r'_data\fromtprogs2tec.dat')
    """
    with open(os.path.join(myfile), "rb") as myinfo:
        alldata = np.fromfile(myinfo, dtype='int32', count=-1)
    # Remove headers (10 positions)
    alldata = alldata[9:-1]
    # Reshape if True, and/or remove marked data if mark_data is False
    if reshape:
        alldata = alldata.reshape(ny_el, nx_el, nz_el)
    elif not mark_data:
        if not reshape:
            alldata = np.abs(alldata)
        elif reshape:
            alldata = np.abs(alldata.reshape(ny_el, nx_el, nz_el))
    print(f'OK reading data from << {myfile} >> (TPROGS bin file) ')
    if not tp_arrays:
        return alldata
    elif tp_arrays:
        # Generate grid nodes in X,Y and Z dims:
        xxx = np.tile(np.arange(0, nx_el + 1, dx), (ny_el + 1) * (nz_el + 1))
        zzz = np.sort(np.tile(np.arange(0, nz_el + 1, dz), (nx_el + 1) * (ny_el + 1)))
        for cur_lay in range(0, nz_el + 1): # todo: to be optimized
            if cur_lay == 0:
                yyy = np.zeros((nx_el + 1,))
            else:
                yyy = np.r_[yyy, np.zeros((nx_el + 1,))]
            for ii in range(1, (ny_el + 1), dy):
                yyy = np.r_[yyy, np.ones((nx_el + 1,)) * ii]
        return alldata, np.c_[xxx,yyy,zzz]


def rd_pfb(myfile, full_output=False):
    """ Read binary Parflow pfb files. Original matlab version from Jehan Rihani, UCB,
    Modified by Daniel Erdal, Uni Tubingen.
    :param myfile:      str, full path pointing to the file the pfb file of interest
    :param full_output: bool, return additional file information if True. Default is False
    :return:  dataset, numpy array
           If full_output is True, also returns:
                n_sgrid:     int, number of sub grids
                (x0,y0,z0):  tuple float, origin of exact geometry domain (note: not computational grid!)
                (nx,ny,nz):  tuple int, number of elements in x,y,z directions (note: not computational grid!)
                (dx,dy,dz):  tuple float, element size along x,y,z axes of geometry domain (not computational grid!)
    :example:
    >>> import plotTools as myplt
    >>> dataPath = os.path.abspath(os.path.join('.', "_data"))
    >>> filename = "Kdistr_rockHeteroTest1.pfb"
    # if full_output = False:
    >>> mydata = rd_pfb(os.path.join(dataPath, filename), full_output=False)
    # if full_output = True:
    >>> [mydata, (x0,y0,z0), (nx,ny,nz), (dx,dy,dz)] = rd_pfb(os.path.join(dataPath, filename), full_output=True)
    # To plot
    >>> myplt.plotimshow(mydata[:, :, 24], title='Slice 25', minval=np.min(mydata[:,:,24]),maxval=np.max(mydata[:,:,24]))
    """

    with open(os.path.join(myfile), "rb") as myinfo:
        x0 = np.fromfile(myinfo, dtype='>d', count=1)
        y0 = np.fromfile(myinfo, dtype='>d', count=1)
        z0 = np.fromfile(myinfo, dtype='>d', count=1)

        nx = np.fromfile(myinfo, dtype='>i4', count=1)
        ny = np.fromfile(myinfo, dtype='>i4', count=1)
        nz = np.fromfile(myinfo, dtype='>i4', count=1)

        dx = np.fromfile(myinfo, dtype='>d', count=1)
        dy = np.fromfile(myinfo, dtype='>d', count=1)
        dz = np.fromfile(myinfo, dtype='>d', count=1)

        n_sgrid = np.fromfile(myinfo, dtype='>i4', count=1)

        mydataset = np.empty((ny[0], nx[0], nz[0]))

        # Loop over the subgrids:
        for cur_sgrid in range(0, n_sgrid[0]):
            ix = np.fromfile(myinfo, dtype='>i4', count=1)
            iy = np.fromfile(myinfo, dtype='>i4', count=1)
            iz = np.fromfile(myinfo, dtype='>i4', count=1)

            nnx = np.fromfile(myinfo, dtype='>i4', count=1)
            nny = np.fromfile(myinfo, dtype='>i4', count=1)
            nnz = np.fromfile(myinfo, dtype='>i4', count=1)

            rx = np.fromfile(myinfo, dtype='>i4', count=1)
            ry = np.fromfile(myinfo, dtype='>i4', count=1)
            rz = np.fromfile(myinfo, dtype='>i4', count=1)

            # Read the actual data by subgrids:
            cur_data = np.fromfile(myinfo, dtype='>d', count=nnx[0]*nny[0]*nnz[0])

            basket = 0
            for cur_z in range(iz[0], iz[0]+nnz[0]):

                for cur_y in range(iy[0], iy[0]+nny[0]):

                    for cur_x in range(ix[0], ix[0]+nnx[0]):

                        mydataset[cur_y, cur_x, cur_z] = cur_data[basket]
                        basket += 1
    if full_output is False:
        return mydataset
    elif full_output is True:
        return mydataset, (x0,y0,z0), (nx,ny,nz), (dx,dy,dz)


def wr_totecplot(nodal_data, elemental_data, nodes_in_x=357, nodes_in_y=185, nodes_in_z=2, var_names=['X','Y','Z'], outputfile='gridtecplot.dat'):
    """ Write nodal and elemental data into ASCII file for TECPLOT. Supports only FEBRICK type of grids.
    Origin is [X=0,Y=0,Z=0}, advancing first on X, then Y and then Z.
    :param nodal_data:      np.array, all nodal data. AT least should have X,Y values
    :param elemental_data:  np.array, all elemental data. AT least one data series
    :param nodes_in_x:      int, number of nodes in X direction
    :param nodes_in_y:      int, number of nodes in Y direction
    :param nodes_in_z:      int, number of nodes in Z direction
    :param var_names:       list, string with names of nodal and elemental values
    :param outputfile:      str, path and filename of the ASCII file
    :return:    ASCII file for TECPLOT stored in given path/name
    :example:
        >>> nnodes, nelem, nodal_data, element_nodes = rd_tcplotmesh(os.path.abspath(os.path.join('.', "_data", "meshtecplot.dat")), fulloutput=True, gridformat='tecplot')
        >>> nodal_data = np.c_[nodal_data, nodal_data[:,0]]
        >>> elemental_data = np.loadtxt(os.path.abspath(os.path.join('.', "_data", "kkkGaus.dat")), usecols=(1,))
        >>> nodal_data[70000:100000, 2] = np.random.uniform(low=1, high=1.5, size=(30000,))
        >>> wr_totecplot(nodal_data, elemental_data, nodes_in_x=357, nodes_in_y=185, nodes_in_z=2, var_names=['X','Y','Z','XX','k'], outputfile=os.path.abspath(os.path.join('.', "_data", "gridtecplot.dat")))
    """
    # Define necessary variables
    el_number = len(elemental_data)
    node_number = len(nodal_data)

    if elemental_data.ndim == 1:
        elemental_data = elemental_data[:,np.newaxis]

    num_of_nodal_data = nodal_data.shape[-1]
    num_of_elemental_data = elemental_data.shape[-1]
    assert len(var_names) == num_of_nodal_data + num_of_elemental_data, 'Dimension mismatch between var_name and actual variable numbers in arrays'

    # Find an appropiate dims to reshape matrix
    node_lines, el_lines = find_exact_shape(node_number, el_number)
    # Start writting headers in ASCII file: set for an FEBRICK type of grid
    file_obj = open(outputfile, 'w')
    file_obj.write('VARIABLES = ')
    for cur_var in var_names:
        file_obj.write('"%s", ' % cur_var)
    file_obj.write('\n')
    file_obj.write('ZONE T=FE, N=%d, E=%d, ZONETYPE=FEBRICK, DATAPACKING=BLOCK,\n' %(node_number, el_number))
    file_obj.write('VARLOCATION=(')

    for cur_varloc in range(1, num_of_nodal_data + 1):
        file_obj.write('[%d]=NODAL, ' % cur_varloc)

    if isinstance(elemental_data, np.ndarray):
        for cur_varloc in range(num_of_nodal_data + 1, num_of_elemental_data + num_of_nodal_data + 1):
            file_obj.write('[%d]=CELLCENTERED, ' % cur_varloc)
    file_obj.write(')\n')

    # Write nodal data:
    for cur_nodalvar in range(0, num_of_nodal_data):
        temp_reshape = nodal_data[:, cur_nodalvar].reshape(node_lines, -1)
        for ii in range(0, node_lines):
            file_obj.write(' '.join(str(temp_reshape[ii, :]).strip('[]').split()) + '\n')
    # Write element data
    for cur_elemvar in range(0, num_of_elemental_data):
        temp_reshape = elemental_data[:, cur_elemvar].reshape(el_lines, -1)
        for jj in range(0, el_lines):
            file_obj.write(' '.join(str(temp_reshape[jj, :]).strip('[]').split()) + '\n')

    # Write connectivity list: nodes corresponding to each mesh element
    # Auxiliary counters
    basketx, baskety, basket1, counter= 1, 1, 0, 1
    for cur_elem in range(1, node_number+1):
        if counter > el_number: # Exit if reached total number of elements.
            break
        else:
            if cur_elem == nodes_in_x * basketx:    # Jump one node if end of X axis
                basketx += 1
                basket1 += 1
            else:
                counter += 1
                basket1 += 1
                node1 = basket1
                node2 = node1+1
                node3 = node1+nodes_in_x
                node4 = node1+nodes_in_x+1
                node5 = node1+(nodes_in_x*nodes_in_y)
                node6 = node5 + 1
                node7 = node5 + nodes_in_x
                node8 = node7 + 1
                file_obj.write('%d %d %d %d %d %d %d %d\n' %(node1, node2, node4, node3, node5, node6, node8, node7))

            if node4 == (nodes_in_x*nodes_in_y*baskety): # Jump one set of nodes along X if at end of Y axis
                baskety += 1
                basket1 += nodes_in_x
                node4 = None
    file_obj.close()
    print('Ok storing data in << %s >> with ASCII Tecplot format' % outputfile)
    return


def wr_sa(mydata, domaingeom=(100, 500, 50), full_domain=False, zlayers=[49]):
    """ Write categorical data in *.sa ASCII format for Parflow.
    :param mydata:      np.array, categorical data to be stored in *.sa format
    :param domaingeom:  tuple, exact dimensions of model domain (note: not computational grid!)
    :param full_domain: bool, write data of full domain if True
    :param zlayers:     int, list. Layers to be consider if full_domain is False
    :return: out_indicators with categorical data of each element in the proper order for Parflow. If full_domain is false
    returns the categories of the selected layers and zeros everywhere else.
    :example:
    >>> import scipy.io
    >>> exampleData = scipy.io.loadmat(os.path.abspath(os.path.join('.', '_data', 'LUV.mat')))
    >>> exampleData = exampleData['LUV_v1']
    >>> myindicators = wr_sa(exampleData, domainGeom=(100, 500, 50), full_domain=False, zlayers=[48, 49])
    # Try with full domain matrix:
    >>> exampleData = scipy.io.loadmat(os.path.abspath(os.path.join('LUV_python.mat')))
    >>> exampleData = exampleData['LUV_v1']
    >>> myindicators = wr_sa(exampleData, domainGeom=(100, 500, 50), full_domain=True, zlayers=None)
    """
    out_indicators = np.zeros((domaingeom[0] * domaingeom[1] * domaingeom[2], 1))

    if full_domain is False:
        indicators = np.zeros(domaingeom)
        if len(zlayers) == 1:
            indicators[:, :, zlayers.pop()] = mydata

        elif len(zlayers) > 1:
            for cur_id, cur_layer in enumerate(zlayers):
                indicators[:, :, cur_layer] = mydata[:, :, cur_id]

    basket = 0
    for cur_z in range(0, domaingeom[2]):  # Loop over Z elements
        for cur_x in range(0, domaingeom[0]):  # Loop over X elements
            for cur_y in range(0, domaingeom[1]):  # Loop over Y elements
                if full_domain is False:
                    out_indicators[basket] = indicators[cur_x, cur_y, cur_z]
                if full_domain is True:
                    out_indicators[basket] = mydata[cur_x, cur_y, cur_z]
                basket += 1

    return out_indicators


def rsh_hyvr_totecplot(mydata, nx_el, ny_el, nz_el):
    """ Reshape three dimensional arrays generated by hyvr for tecplot.
    :param mydata: 3D np.array, data to reshape
    :param nx_el:   int, number of elements in X dimension
    :param ny_el:   int, number of elements in Y dimension
    :param nz_el:   int, number of elements in Z dimension
    :return: 1D np.array with data order for tecplot.
    :example:
    >>> import hyvr_jb
    >>> nyel, nxel, nzel = 40, 80, 110
    >>> myfile = r'_data\made_hyvr.dat'
    >>> mydata = hyvr_jb.utils.load_pickle(myfile)
    >>> my_arch_elem = mydata['ae']
    >>> my_arch_elem_reshaped = rsh_hyvr_totecplot(my_arch_elem, nxel, nyel, nzel)
    """
    # Create support arrays:
    vartemp = np.empty([ny_el, nx_el])
    varbasket = np.zeros([1])
    for ii in range(0, nz_el):
        vartemp[0:ny_el, 0:nx_el] = mydata[0:nx_el, 0:ny_el, ii].T
        vartempshaped = np.reshape(vartemp, ny_el * nx_el, order='A')
        varbasket = np.r_[(varbasket, vartempshaped)]  # This array has the order needed in HGS

    return varbasket[1:]


# todo: function to write a grok file for grid generation, and run grok, delete extra files and create batch
# Here is for testing functions:
# test reading a mesh file from tp2018
myfile = r'_data\small_hyvr_tp2018.mesh'
ids, next_str = str_infile(myfile, '#', index=True)
tempfile = open(myfile, 'r')
content = tempfile.readlines()
grid_coord = np.empty((nnodes, 3))
# select coordinates of each dimension and change to numpy array: e.g. X, = content[ids[0]+1:ids[1]]
# select the connectivity list and to numpy array: connlist = content[-nel:]
print('Pause')