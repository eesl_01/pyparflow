"""
Main caller. Reads settings from the control file
Created on Thursday Oct 12 2017
@author: Emilio Sanchez-Leon#

"""

import numpy as np
import os, io_utils as io
import struct
import matplotlib as mpl

import matplotlib.pylab as plt





dataPath = os.path.abspath(os.path.join('.', '_datafiles'))
filename = 'rurlaf.out.press.02754.pfb'      # 'Kdistr_rockHeteroTest1.pfb' 'rurlaf.out.press.02754.pfb' 'Kdistr_rockHeteroTest1.pfb'

mydata = io.rd_pfb(os.path.join(dataPath, filename), full_output=False)
mydata, (x0,y0,z0), (nx,ny,nz), (dx,dy,dz) = rd_pfb(os.path.join(dataPath, filename), full_output=True)

fig = plt.figure(1)
ax1 = fig.add_subplot(111)
ax1.set_title('XY Slice @Z #25')
im1 = ax1.imshow(mydata[:, :, 24].T, origin='lower')
ax1.set_xlabel('X coordinate', fontsize='x-small')
ax1.set_ylabel('Y coordinate', fontsize='x-small')
norm = mpl.colors.Normalize(vmin=np.min(mydata), vmax=np.max(mydata))
im1.set_norm(norm)
cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
cb1.set_label('Pressure head', fontsize='x-small')

plt.figure(2)
plt.title('XY Slice @Z #50')
plt.imshow(mydata[:, :, -1], origin='lower')
plt.xlabel('X coordinate', fontsize='x-small')
plt.ylabel('Y coordinate', fontsize='x-small')
plt.grid()
plt.colorbar()
# myplt.plotimshow(mydata[:, :, 24], '', '', points=np.array([]), title='Slice 25', minval=np.min(mydata[:,:,24]),
#                  maxval=np.max(mydata[:,:,24]), xlim=None, ylim=None, xticks=None, yticks=None)


print('Pause')


# Try to find deoth to GWT
z = np.ones((50,1))*0.1
z[0]=1.8; z[1]=1.7; z[2:6]=1.5; z[6]=1.3
z[-1]=0.2; z[-2]=0.3; z[-6:-1]=0.5; z[-7]=0.7;
z=z*0.1