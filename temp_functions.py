"""
Tecplot IO functions. Useful for plotting PARFLOW-CLM, TPROGS & HYVR.
Created on Tuesday Feb 02 2018
@author: Emilio Sanchez-Leon, Uni Tubingen
@comments: Check to do list!
"""
import numpy as np
import os, re

# This reorders and stores output from HyVR for Tecplot
#def reorder_totecplot():
